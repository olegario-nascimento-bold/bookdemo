package com.example.bookdemo.common.mappers;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public interface Mapper<From, To> {

    @Nullable
    To map(@Nullable From value);

    @NonNull
    default List<To> map(@Nullable List<From> values) {
        if (values == null) return new ArrayList<>(0);

        List<To> returnValues = new ArrayList<>(values.size());
        for (From value : values) {
            returnValues.add(map(value));
        }
        return returnValues;
    }

}