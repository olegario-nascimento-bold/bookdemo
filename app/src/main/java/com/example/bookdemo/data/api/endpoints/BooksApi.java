package com.example.bookdemo.data.api.endpoints;

import com.example.bookdemo.data.api.models.response.BookRemote;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;

public interface BooksApi {

    String PREFIX = "books.json";

    @GET(PREFIX)
    Single<Response<List<BookRemote>>> getBooks();
}
