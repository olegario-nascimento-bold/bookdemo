package com.example.bookdemo.data.api.exceptions.http;

import com.example.bookdemo.data.api.exceptions.base.BaseException;
import com.example.bookdemo.data.api.services.base.ApiError;

public class UnauthorizedException extends BaseException {

    public UnauthorizedException(ApiError apiError) {
        super(apiError);
    }

    public UnauthorizedException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}