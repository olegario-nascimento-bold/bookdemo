package com.example.bookdemo.data.api.models.response;

import com.google.gson.annotations.SerializedName;

public class BookRemote {
    @SerializedName("title")
    private String title;
    @SerializedName("author")
    private String author;
    @SerializedName("imageURL")
    private String imageURL;

    public BookRemote() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
