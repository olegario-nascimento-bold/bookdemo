package com.example.bookdemo.data.api.exceptions.http;

import com.example.bookdemo.data.api.exceptions.base.BaseException;
import com.example.bookdemo.data.api.services.base.ApiError;

public class InternetConnectionException extends BaseException {

    public InternetConnectionException(ApiError apiError) {
        super(apiError);
    }

    public InternetConnectionException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}
