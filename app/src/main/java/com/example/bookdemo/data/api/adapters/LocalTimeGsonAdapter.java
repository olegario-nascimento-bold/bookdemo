package com.example.bookdemo.data.api.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class LocalTimeGsonAdapter extends TypeAdapter<LocalTime> {
    @Override
    public void write(JsonWriter out, LocalTime value) throws IOException {
        out.value(value.format(getFormatter()));
    }

    @Override
    public LocalTime read(JsonReader in) throws IOException {
        try {
            return LocalTime.parse(in.nextString(), getFormatter());
        } catch (DateTimeParseException exception) {
            //exception.printStackTrace();
            return LocalTime.now();
        } catch (IllegalStateException ise) {
            //ise.printStackTrace();
            return LocalTime.now();
        }
    }

    private DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern("HH:mm");
    }
}