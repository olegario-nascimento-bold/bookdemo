package com.example.bookdemo.data.api.exceptions.http;

import com.example.bookdemo.data.api.exceptions.base.BaseException;
import com.example.bookdemo.data.api.services.base.ApiError;

public class NotFoundException extends BaseException {

    public NotFoundException(ApiError apiError) {
        super(apiError);
    }

    public NotFoundException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}
