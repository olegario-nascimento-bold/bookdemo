package com.example.bookdemo.data.api.services;

import com.example.bookdemo.data.api.endpoints.BooksApi;
import com.example.bookdemo.data.api.models.response.BookRemote;
import com.example.bookdemo.data.api.services.base.ApiService;
import com.example.bookdemo.utils.ResourceProvider;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;
import retrofit2.Retrofit;

@Singleton
public class BooksService extends ApiService {

    private BooksApi api;

    @Inject
    public BooksService(Retrofit retrofit, ResourceProvider resourceProvider, Gson gson) {
        super(retrofit, resourceProvider, gson);
        this.api = retrofit.create(BooksApi.class);
    }

    public Single<List<BookRemote>> getListOfBooks() {
        return api.getBooks()
                .compose(networkMapTransform());
    }
}