package com.example.bookdemo.data.api.exceptions.api;

import com.example.bookdemo.data.api.exceptions.base.BaseException;
import com.example.bookdemo.data.api.services.base.ApiError;

public class UnknownErrorException extends BaseException {
    public UnknownErrorException(ApiError apiError) {
        super(apiError);
    }

    public UnknownErrorException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}
