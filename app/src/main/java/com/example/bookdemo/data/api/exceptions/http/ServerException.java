package com.example.bookdemo.data.api.exceptions.http;

import com.example.bookdemo.data.api.exceptions.base.BaseException;
import com.example.bookdemo.data.api.services.base.ApiError;

public class ServerException extends BaseException {

    public ServerException(ApiError apiError) {
        super(apiError);
    }

    public ServerException(Throwable throwable, ApiError apiError) {
        super(throwable, apiError);
    }
}
