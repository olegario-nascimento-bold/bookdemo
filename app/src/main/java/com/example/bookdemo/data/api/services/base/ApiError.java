package com.example.bookdemo.data.api.services.base;

import androidx.annotation.NonNull;

public class ApiError {

    private String message;

    public ApiError(@NonNull String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
