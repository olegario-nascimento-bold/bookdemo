package com.example.bookdemo.domain.managers;

import com.example.bookdemo.data.api.services.BooksService;
import com.example.bookdemo.domain.mappers.BookMapper;
import com.example.bookdemo.domain.models.Book;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class BooksManager {
    private final BooksService service;
    private BookMapper bookMapper;

    @Inject
    BooksManager(BooksService service, BookMapper bookMapper) {
        this.service = service;
        this.bookMapper = bookMapper;
    }

    public Single<List<Book>> getBooks() {
        return service.getListOfBooks()
                .map(bookMapper::map);
    }
}
