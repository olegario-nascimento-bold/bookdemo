package com.example.bookdemo.domain.mappers;

import androidx.annotation.Nullable;

import com.example.bookdemo.common.mappers.Mapper;
import com.example.bookdemo.data.api.models.response.BookRemote;
import com.example.bookdemo.domain.models.Book;

import javax.inject.Inject;

public class BookMapper implements Mapper<BookRemote, Book> {

    @Inject
    BookMapper() {
    }

    @Nullable
    @Override
    public Book map(@Nullable BookRemote value) {
        if (value == null) {
            return null;
        }
        Book book = new Book();
        book.setTitle(value.getTitle());
        book.setAuthor(value.getAuthor());
        book.setImageURL(value.getImageURL());
        return book;
    }
}