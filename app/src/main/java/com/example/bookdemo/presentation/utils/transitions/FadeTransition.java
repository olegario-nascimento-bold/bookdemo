package com.example.bookdemo.presentation.utils.transitions;

import com.example.bookdemo.R;
import com.example.bookdemo.presentation.utils.ActivityTransition;

public class FadeTransition extends ActivityTransition {

    public FadeTransition() {
        super(R.anim.enter_fade, R.anim.stay, R.anim.stay, R.anim.exit_fade);
    }
}
