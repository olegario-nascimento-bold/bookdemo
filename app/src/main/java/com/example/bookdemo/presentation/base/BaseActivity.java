package com.example.bookdemo.presentation.base;


import android.content.pm.ActivityInfo;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.bookdemo.BR;
import com.example.bookdemo.R;
import com.example.bookdemo.presentation.utils.ActivityTransition;
import com.example.bookdemo.presentation.utils.actions.UiEvents;
import com.example.bookdemo.presentation.utils.actions.UiMessages;
import com.example.bookdemo.presentation.utils.transitions.FadeTransition;
import com.example.bookdemo.presentation.utils.transitions.TransitionManager;
import com.example.bookdemo.utils.ResourceProvider;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseActivity<ViewBinding extends ViewDataBinding, ViewModel extends BaseViewModel> extends DaggerAppCompatActivity {


    @Inject
    ResourceProvider resourceProvider;

    @Inject
    UiEvents uiEvents;

    @Inject
    UiMessages uiMessages;

    @Inject
    ViewModelFactory<ViewModel> viewModelFactory;


    private ViewModel viewModel;
    private ViewBinding binding;
    private boolean isPhoneOnline = true;


    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final CompositeDisposable uiActionsCompositeDisposable = new CompositeDisposable();

    //private final TransitionManager transitionManager = TransitionManager.create(getActivityTransition());

    protected ActivityTransition getActivityTransition() {
        // To override if necessary
        return new FadeTransition();
    }

    protected void initArguments(Bundle args) {
    }

    public abstract int layoutResId();

    protected abstract void initViews(final ViewBinding binding);


    public abstract Class<ViewModel> viewModelClass();

    protected void addDisposable(Disposable d) {
        compositeDisposable.add(d);
    }

    protected void addUiActionsDisposable(Disposable d) {
        uiActionsCompositeDisposable.add(d);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        initArguments(getIntent().getExtras());

       // transitionManager.enterTransition(this);
        super.onCreate(savedInstanceState);

        setContentView(layoutResId());
        ButterKnife.bind(this);

        binding = DataBindingUtil.setContentView(this, layoutResId());
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClass());

        initBinding(binding, this.viewModel);
        binding.executePendingBindings();
        initViews(binding);

        initSavedInstanceState(savedInstanceState);
    }

    protected void initSavedInstanceState(Bundle savedInstanceState) {
    }

    @Override
    @CallSuper
    protected void onStart() {
        super.onStart();
        registerObservables();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerUiActionsObservables();
    }

    @Override
    protected void onPause() {
        uiActionsCompositeDisposable.clear();
        super.onPause();
    }

    @Override
    @CallSuper
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    protected void registerObservables() {
    }

    @CallSuper
    protected final void registerUiActionsObservables() {
        uiEvents.observeToastEvent()
                .doOnSubscribe(this::addUiActionsDisposable)
                .subscribe(event -> uiMessages.showToast(event.getMessage()));

        uiEvents.observeCloseEvent()
                .doOnSubscribe(this::addUiActionsDisposable)
                .subscribe(event -> finish());
    }

    @CallSuper
    protected void initBinding(final ViewBinding binding, final ViewModel viewModel) {
        binding.setVariable(BR.viewModel, viewModel);
    }

    @Override
    public void finish() {
        super.finish();
     //   transitionManager.exitTransition(this);
    }

    public final ResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    public final ViewModel getViewModel() {
        return viewModel;
    }

    public final ViewBinding getBinding() {
        return binding;
    }

    protected void changeFragment(Fragment fragment, String tag, int animEnter, int animExit) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(animEnter, animExit, animEnter, animExit)
                .replace(R.id.fragment_container, fragment, tag)
                .commitAllowingStateLoss();
    }

    protected void changeFragmentAddingToBackstack(Fragment fragment, String tag, int animEnter, int animExit) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(animEnter, animExit, animEnter, animExit)
                .addToBackStack(null)
                .replace(R.id.fragment_container, fragment, tag)
                .commitAllowingStateLoss();
    }

    protected void changeFragment(Fragment fragment, String tag) {
        changeFragment(fragment, tag, R.anim.stay, R.anim.exit_fade);
    }

    protected void addFragmentButDontAddToBackstack(Fragment fragment, String tag, int enterAnimaton, int exitAnimation) {
        try {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(enterAnimaton, exitAnimation, enterAnimaton, exitAnimation)
                    .add(R.id.fragment_container, fragment)
                    .commitAllowingStateLoss();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    public <T extends BaseViewModel> T getSharedViewModel(ViewModelProvider.Factory factory, Class<T> clazz) {
        return ViewModelProviders.of(this, factory).get(clazz);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof TextInputEditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) ev.getRawX(), (int) ev.getRawY())) {
                    v.clearFocus();
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public boolean isApplicationOnLine() {
        return true;
    }

}