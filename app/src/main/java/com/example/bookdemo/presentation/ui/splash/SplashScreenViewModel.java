package com.example.bookdemo.presentation.ui.splash;

import android.annotation.SuppressLint;

import com.example.bookdemo.R;
import com.example.bookdemo.common.schedulers.SchedulerProvider;
import com.example.bookdemo.presentation.base.BaseViewModel;
import com.example.bookdemo.presentation.ui.splash.enums.ScreenType;
import com.example.bookdemo.presentation.ui.splash.events.OpenMainEvent;
import com.example.bookdemo.presentation.utils.actions.UiEvents;
import com.example.bookdemo.presentation.utils.actions.events.AnimationCompleteEvent;
import com.example.bookdemo.presentation.utils.actions.events.Event;
import com.example.bookdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashScreenViewModel extends BaseViewModel {

    private static final int SPLASH_DELAY = 2500;


    private BehaviorRelay<OpenMainEvent> openMainObservable = BehaviorRelay.create();
    private BehaviorRelay<AnimationCompleteEvent> animationCompleteObservable = BehaviorRelay.create();

    @Inject
    SplashScreenViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider,
                          UiEvents uiEvents) {
        super(resourceProvider, schedulerProvider, uiEvents);

        openMainObservable.accept(new OpenMainEvent(ScreenType.HOME));
    }

    Observable<OpenMainEvent> observeOpenMain() {
        return openMainObservable;
    }

    public String getSplashAnimationFileName() {
        return getResourceProvider().getString(R.string.app_splash);
    }

    @SuppressLint("CheckResult")
    public void animationCompleted(long animationDuration) {
        Completable.complete()
                .delay(animationDuration + SPLASH_DELAY, TimeUnit.MILLISECONDS)
                .doOnSubscribe(this::addDisposable)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        () ->
                                animationCompleteObservable.accept(new AnimationCompleteEvent()),
                        Timber::e
                );
    }
}