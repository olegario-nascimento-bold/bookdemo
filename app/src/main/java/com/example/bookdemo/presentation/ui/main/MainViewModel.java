package com.example.bookdemo.presentation.ui.main;

import androidx.databinding.Bindable;

import com.example.bookdemo.common.schedulers.SchedulerProvider;
import com.example.bookdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.bookdemo.presentation.utils.actions.UiEvents;
import com.example.bookdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.BehaviorRelay;

import javax.inject.Inject;

public class MainViewModel extends BaseToolbarViewModel {

    @Inject
    MainViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider,
                  UiEvents uiEvents) {
        super(resourceProvider, schedulerProvider, uiEvents);

    }

    @Bindable
    @Override
    public String getToolbarTitle() {
        return "";
    }
}