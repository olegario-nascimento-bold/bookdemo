package com.example.bookdemo.presentation.utils.actions;


import com.jakewharton.rxrelay2.BehaviorRelay;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Holds observables that propagate intents (external actions to affect the app, not Android ones)
 * to other screens.
 */
@Singleton
public class IntentActionManager {

    private BehaviorRelay<IntentAction<Integer>> openMapFilterObservable = BehaviorRelay.create();
    private BehaviorRelay<IntentAction<Boolean>> openNotificationAlertsObservable = BehaviorRelay.create();

    @Inject
    IntentActionManager() {
    }

    public void openMapFilter(int mapFilterId) {
        openMapFilterObservable.accept(IntentAction.of(mapFilterId));
    }

    public Observable<IntentAction<Integer>> observeOpenMapFilterId() {
        return openMapFilterObservable;
    }

    public void openNotificationAlerts(boolean open) {
        openNotificationAlertsObservable.accept(IntentAction.of(open));
    }

    public Observable<IntentAction<Boolean>> observeOpenNotificationsAlerts() {
        return openNotificationAlertsObservable;
    }
}