package com.example.bookdemo.presentation.base.toolbar;


import androidx.annotation.ColorRes;
import androidx.annotation.MenuRes;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.ViewDataBinding;

import com.example.bookdemo.R;
import com.example.bookdemo.presentation.base.BaseFragment;
import com.google.android.material.appbar.AppBarLayout;

public abstract class BaseToolbarFragment<ViewBinding extends ViewDataBinding, ViewModel extends BaseToolbarViewModel>
        extends BaseFragment<ViewBinding, ViewModel> {

    public void initToolbar(Toolbar toolbar, boolean close, @ColorRes int buttonColor) {
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.getNavigationIcon().setTint(getResourceProvider().getColor(buttonColor));

        toolbar.setNavigationOnClickListener(v ->
                onBackPressed());
    }

    public void initToolbarWithoutNavigation(Toolbar toolbar) {
        toolbar.setNavigationOnClickListener(v ->
                onBackPressed());
    }

    public boolean showToolbarNavigation() {
        return true;
    }

    public void initShadowlessCloseToolbarToolbar(Toolbar toolbar, AppBarLayout appBar, @ColorRes int backgroundColor, @ColorRes int buttonColor) {
        initToolbar(toolbar, true, buttonColor);
        appBar.setBackgroundColor(getResourceProvider().getColor(backgroundColor));
        toolbar.getNavigationIcon().setTint(getResourceProvider().getColor(buttonColor));
    }

    public void initShadowlessToolbarToolbar(Toolbar toolbar, AppBarLayout appBar, @ColorRes int backgroundColor, @ColorRes int btnColor) {
        initToolbar(toolbar, false, btnColor);
        appBar.setBackgroundColor(getResourceProvider().getColor(backgroundColor));
    }

    public void initToolbarWithMenu(Toolbar toolbar, boolean close, boolean upNavigationKey, @ColorRes int buttonColor, @MenuRes int menuResId, Toolbar.OnMenuItemClickListener listener) {
        if (upNavigationKey) {
            initToolbar(toolbar, close, buttonColor);
            toolbar.getNavigationIcon().setTint(getResourceProvider().getColor(buttonColor));
        } else {
            initToolbarWithoutNavigation(toolbar);
        }
        toolbar.inflateMenu(menuResId);
        toolbar.setOnMenuItemClickListener(listener);
    }
}