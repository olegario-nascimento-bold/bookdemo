package com.example.bookdemo.presentation.base;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.example.bookdemo.BR;
import com.example.bookdemo.R;
import com.example.bookdemo.utils.ResourceProvider;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BaseFragment<ViewBinding extends ViewDataBinding, ViewModel extends BaseViewModel> extends DaggerFragment {


    @Inject
    ViewModelFactory<ViewModel> viewModelFactory;

    @Inject
    ResourceProvider resourceProvider;

    private ViewModel viewModel;
    private ViewBinding binding;

    public void propagatedOnBackPressed() {
    }

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    protected void initArguments(Bundle args) {
    }

    protected abstract int layoutResId();

    protected abstract Class<ViewModel> viewModelClass();

    protected abstract void initViews(final ViewBinding binding);

    protected void addDisposable(Disposable d) {
        compositeDisposable.add(d);
    }

    @Override
    public void onAttach(Context context) {
        initArguments(getArguments());
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, layoutResId(), container, false);
        return getBinding().getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClass());

        initBinding(binding, this.viewModel);
        initViews(binding);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onStart() {
        super.onStart();
        registerObservables();
    }

    @Override
    @CallSuper
    public void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @CallSuper
    protected void initBinding(final ViewBinding binding, final ViewModel viewModel) {
        binding.setVariable(BR.viewModel, viewModel);
    }

    protected void registerObservables() {
    }

    public final ViewModel getViewModel() {
        return viewModel;
    }

    public final ViewBinding getBinding() {
        return binding;
    }

    public final BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    @SuppressWarnings("ConstantConditions")
    public <T extends BaseViewModel> T getActivityViewModel(Class<T> clazz) {
        return ViewModelProviders.of(getActivity()).get(clazz);
    }

    @SuppressWarnings("ConstantConditions")
    public <T extends BaseViewModel> T getParentViewModel(Class<T> clazz) {
        Fragment parentFragment = getParentFragment();
        if (parentFragment == null) {
            return ViewModelProviders.of(getActivity()).get(clazz);
        } else {
            return ViewModelProviders.of(parentFragment).get(clazz);
        }
    }

    @SuppressWarnings("ConstantConditions")
    public <T extends BaseViewModel> T getSharedViewModel(ViewModelProvider.Factory factory, Class<T> clazz) {
        return ViewModelProviders.of(getActivity(), factory).get(clazz);
    }

    public final ResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    protected void changeFragment(Fragment fragment, String tag, int enterAnimation, int exitAnimation) {
        try {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(enterAnimation, exitAnimation, enterAnimation, exitAnimation)
                    .replace(R.id.fragment_container, fragment, tag)
                    .commitAllowingStateLoss();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    protected void changeChildFragment(Fragment fragment, String tag, int enterAnimation, int exitAnimation) {
        try {
            getChildFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(enterAnimation, exitAnimation, enterAnimation, exitAnimation)
                    .replace(R.id.fragment_container, fragment, tag)
                    .commitAllowingStateLoss();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    protected void addFragment(Fragment fragment, String tag, int enterAnimaton, int exitAnimation) {
        try {
            requireActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(enterAnimaton, exitAnimation, enterAnimaton, exitAnimation)
                    .addToBackStack(null)
                    .add(R.id.fragment_container, fragment, tag)
                    .commitAllowingStateLoss();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    protected void onBackPressed() {
        requireActivity().onBackPressed();
    }

}