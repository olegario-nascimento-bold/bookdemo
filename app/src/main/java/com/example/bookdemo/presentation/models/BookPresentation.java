package com.example.bookdemo.presentation.models;

import android.os.Parcel;
import android.os.Parcelable;

public class BookPresentation implements Parcelable {
    private String title;
    private String author;
    private String imageURL;

    public BookPresentation() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.author);
        dest.writeString(this.imageURL);
    }

    protected BookPresentation(Parcel in) {
        this.title = in.readString();
        this.author = in.readString();
        this.imageURL = in.readString();
    }

    public static final Creator<BookPresentation> CREATOR = new Creator<BookPresentation>() {
        @Override
        public BookPresentation createFromParcel(Parcel source) {
            return new BookPresentation(source);
        }

        @Override
        public BookPresentation[] newArray(int size) {
            return new BookPresentation[size];
        }
    };
}
