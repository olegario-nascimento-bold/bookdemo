package com.example.bookdemo.presentation.ui.splash.events;

import com.example.bookdemo.presentation.ui.splash.enums.ScreenType;
import com.example.bookdemo.presentation.utils.actions.events.NavigationEvent;

public class OpenMainEvent extends NavigationEvent {
    public ScreenType screenType;

    public OpenMainEvent(ScreenType screenType) {
        this.screenType = screenType;
    }

    public ScreenType getScreenType() {
        return screenType;
    }
}
