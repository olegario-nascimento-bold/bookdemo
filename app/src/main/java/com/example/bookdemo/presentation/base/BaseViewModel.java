package com.example.bookdemo.presentation.base;


import androidx.annotation.CallSuper;
import androidx.databinding.Bindable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.lifecycle.ViewModel;
import androidx.databinding.Observable;

import com.example.bookdemo.common.schedulers.SchedulerProvider;
import com.example.bookdemo.presentation.utils.actions.UiEvents;
import com.example.bookdemo.utils.ResourceProvider;

import io.reactivex.CompletableTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseViewModel extends ViewModel implements Observable {

    private final ResourceProvider resourceProvider;
    private final SchedulerProvider schedulerProvider;
    private final UiEvents uiEvents;
    private boolean fromSettingsScreen = false;

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    public BaseViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider,
                         UiEvents uiEvents) {
        this.resourceProvider = resourceProvider;
        this.schedulerProvider = schedulerProvider;
        this.uiEvents = uiEvents;
    }

    protected ResourceProvider getResourceProvider() {
        return resourceProvider;
    }

    protected SchedulerProvider getSchedulerProvider() {
        return schedulerProvider;
    }

    protected UiEvents getUiEvents() {
        return uiEvents;
    }

    protected void addDisposable(Disposable d) {
        compositeDisposable.add(d);
    }

    @Override
    @CallSuper
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    //<editor-fold desc="Rx transforms">
    protected <T> ObservableTransformer<T, T> schedulersTransformObservable() {
        return upstream -> upstream
                .observeOn(schedulerProvider.ui());
    }

    protected <T> SingleTransformer<T, T> schedulersTransformSingle() {
        return upstream -> upstream
                .observeOn(schedulerProvider.ui());
    }

    protected CompletableTransformer schedulersTransformCompletable() {
        return upstream -> upstream
                .observeOn(schedulerProvider.ui());
    }

    //</editor-fold>

    //<editor-fold desc="BaseObservable">
    private transient PropertyChangeRegistry mCallbacks;

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        synchronized (this) {
            if (mCallbacks == null) {
                mCallbacks = new PropertyChangeRegistry();
            }
        }
        mCallbacks.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        synchronized (this) {
            if (mCallbacks == null) {
                return;
            }
        }
        mCallbacks.remove(callback);
    }

    /**
     * Notifies listeners that all properties of this instance have changed.
     */
    public void notifyChange() {
        synchronized (this) {
            if (mCallbacks == null) {
                return;
            }
        }
        mCallbacks.notifyCallbacks(this, 0, null);
    }

    /**
     * Notifies listeners that a specific property has changed. The getter for the property
     * that changes should be marked with {@link Bindable} to generate a field in
     * <code>BR</code> to be used as <code>fieldId</code>.
     *
     * @param fieldId The generated BR id for the Bindable field.
     */
    public void notifyPropertyChanged(int fieldId) {
        synchronized (this) {
            if (mCallbacks == null) {
                return;
            }
        }
        mCallbacks.notifyCallbacks(this, fieldId, null);
    }

    public void notifyPropertiesChanged(int... fieldIds) {
        for (int fieldId : fieldIds) {
            notifyPropertyChanged(fieldId);
        }
    }
    //</editor-fold>


}