package com.example.bookdemo.presentation.utils.actions;


import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bookdemo.di.scopes.ActivityScope;

import javax.inject.Inject;

@ActivityScope
public class UiMessages {

    private AppCompatActivity mActivity;

    public interface OkDialogCallback {
        void onDismiss();
    }

    public interface BooleanDialogCallback {
        void onAction(Boolean result);
    }

    @Inject
    public UiMessages(AppCompatActivity activity) {
        mActivity = activity;
    }

    public void showToast(String message) {
        Toast.makeText(mActivity, message, Toast.LENGTH_LONG).show();
    }
}