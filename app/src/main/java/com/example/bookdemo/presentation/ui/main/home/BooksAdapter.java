package com.example.bookdemo.presentation.ui.main.home;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.example.bookdemo.databinding.ListItemAppBinding;
import com.example.bookdemo.domain.models.Book;
import com.example.bookdemo.presentation.base.adapters.BindableDiffAdapter;
import com.example.bookdemo.presentation.base.adapters.BindableViewHolder;
import com.example.bookdemo.presentation.models.BookPresentation;

public class BooksAdapter extends BindableDiffAdapter<BookPresentation> {

    BooksAdapter() {
    }

    @NonNull
    @Override
    public BindableViewHolder<? extends BookPresentation, ? extends ViewDataBinding> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemAppBinding binding = ListItemAppBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new BookViewHolder(binding);
    }

    private class BookViewHolder extends BindableViewHolder<BookPresentation, ListItemAppBinding> {

        BookViewHolder(ListItemAppBinding binding) {
            super(binding);
        }
    }
}