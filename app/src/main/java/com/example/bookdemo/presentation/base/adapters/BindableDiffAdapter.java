package com.example.bookdemo.presentation.base.adapters;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

public abstract class BindableDiffAdapter<T> extends ListAdapter<T, BindableViewHolder<? extends T, ? extends ViewDataBinding>> {

    public BindableDiffAdapter() {
        super(defaultDiffCallback());
    }

    public BindableDiffAdapter(@NonNull DiffUtil.ItemCallback<T> diffCallback) {
        super(diffCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull BindableViewHolder<? extends T, ? extends ViewDataBinding> holder, int position) {
        holder.bindInternal(getItem(position));
    }

    @CallSuper
    @Override
    public void onViewRecycled(@NonNull BindableViewHolder<? extends T, ? extends ViewDataBinding> holder) {
        holder.unbind();
    }

    private static <T> DiffUtil.ItemCallback<T> defaultDiffCallback() {
        return new DiffUtil.ItemCallback<T>() {
            @Override
            public boolean areItemsTheSame(T oldItem, T newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areContentsTheSame(T oldItem, T newItem) {
                return oldItem.equals(newItem);
            }
        };
    }
}