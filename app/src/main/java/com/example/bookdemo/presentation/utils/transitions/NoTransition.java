package com.example.bookdemo.presentation.utils.transitions;

import com.example.bookdemo.presentation.utils.ActivityTransition;

public class NoTransition extends ActivityTransition {

    public NoTransition() {
        super(0, 0, 0, 0);
    }
}