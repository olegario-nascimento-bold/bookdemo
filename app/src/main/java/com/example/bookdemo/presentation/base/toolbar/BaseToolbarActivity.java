package com.example.bookdemo.presentation.base.toolbar;


import androidx.annotation.ColorRes;
import androidx.annotation.MenuRes;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.ViewDataBinding;

import com.example.bookdemo.R;
import com.example.bookdemo.presentation.base.BaseActivity;
import com.google.android.material.appbar.AppBarLayout;

public abstract class BaseToolbarActivity<ViewBinding extends ViewDataBinding, ViewModel extends BaseToolbarViewModel>
        extends BaseActivity<ViewBinding, ViewModel> {

    public void initToolbar(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }


    public void changeToolbarMenu(Toolbar toolbar, @ColorRes int buttonColor, @MenuRes int menuResId, Toolbar.OnMenuItemClickListener listener) {
        toolbar.getMenu().clear();
        toolbar.inflateMenu(menuResId);
        toolbar.setOnMenuItemClickListener(listener);
        toolbar.getNavigationIcon().setTint(getResourceProvider().getColor(buttonColor));
    }

    public void initToolbar(Toolbar toolbar, @ColorRes int buttonColor) {
        initToolbar(toolbar);
        toolbar.getNavigationIcon().setTint(getResourceProvider().getColor(buttonColor));
    }

}