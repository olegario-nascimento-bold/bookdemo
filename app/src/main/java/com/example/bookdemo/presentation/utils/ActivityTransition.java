package com.example.bookdemo.presentation.utils;


import androidx.annotation.AnimRes;

public abstract class ActivityTransition {

    @AnimRes
    private int mStartEnterAnim;

    @AnimRes
    private int mStartExitAnim;

    @AnimRes
    private int mFinishEnterAnim;

    @AnimRes
    private int mFinishExitAnim;

    public ActivityTransition(@AnimRes int startEnterAnim, @AnimRes int startExitAnim, @AnimRes int finishEnterAnim, @AnimRes int finishExitAnim) {
        mStartEnterAnim = startEnterAnim;
        mStartExitAnim = startExitAnim;
        mFinishEnterAnim = finishEnterAnim;
        mFinishExitAnim = finishExitAnim;
    }

    public int getStartEnterAnim() {
        return mStartEnterAnim;
    }

    public void setStartEnterAnim(@AnimRes int startEnterAnim) {
        mStartEnterAnim = startEnterAnim;
    }

    public int getStartExitAnim() {
        return mStartExitAnim;
    }

    public void setStartExitAnim(@AnimRes int startExitAnim) {
        mStartExitAnim = startExitAnim;
    }

    public int getFinishEnterAnim() {
        return mFinishEnterAnim;
    }

    public void setFinishEnterAnim(@AnimRes int finishEnterAnim) {
        mFinishEnterAnim = finishEnterAnim;
    }

    public int getFinishExitAnim() {
        return mFinishExitAnim;
    }

    public void setFinishExitAnim(@AnimRes int finishExitAnim) {
        mFinishExitAnim = finishExitAnim;
    }
}
