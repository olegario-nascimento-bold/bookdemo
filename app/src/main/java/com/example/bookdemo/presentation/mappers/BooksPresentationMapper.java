package com.example.bookdemo.presentation.mappers;

import com.example.bookdemo.domain.models.Book;
import com.example.bookdemo.presentation.models.BookPresentation;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BooksPresentationMapper {
    BooksPresentationMapper INSTANCE = Mappers.getMapper(BooksPresentationMapper.class);

    //map to domain
    BookPresentation appsDomainToPresentation(Book appsDomain);
    List<BookPresentation> listOfAppsDomainToPresentation(List<Book> bookDomain);

}