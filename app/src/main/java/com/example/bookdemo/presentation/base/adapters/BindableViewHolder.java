package com.example.bookdemo.presentation.base.adapters;


import androidx.annotation.CallSuper;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BindableViewHolder<T, ViewBinding extends ViewDataBinding> extends RecyclerView.ViewHolder {

    protected final ViewBinding binding;

    protected T item;

    public BindableViewHolder(ViewBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    @SuppressWarnings("unchecked")
    @CallSuper
    final <S> void bindInternal(S item) {
        this.item = (T) item;

        binding.setVariable(BR.listItem, item);
        binding.executePendingBindings();

        bind(this.item);
    }

    public void bind(T item) {}

    public void unbind() {}
}
