package com.example.bookdemo.presentation.ui.main.home;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.example.bookdemo.R;
import com.example.bookdemo.common.schedulers.SchedulerProvider;
import com.example.bookdemo.domain.managers.BooksManager;
import com.example.bookdemo.presentation.base.toolbar.BaseToolbarViewModel;
import com.example.bookdemo.presentation.mappers.BooksPresentationMapper;
import com.example.bookdemo.presentation.models.BookPresentation;
import com.example.bookdemo.presentation.utils.actions.UiEvents;
import com.example.bookdemo.utils.ResourceProvider;
import com.jakewharton.rxrelay2.BehaviorRelay;


import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class HomeFragmentViewModel extends BaseToolbarViewModel {
    private final BooksManager booksManager;
    private BehaviorRelay<List<BookPresentation>> booksObservable = BehaviorRelay.create();

    @Inject
    public HomeFragmentViewModel(ResourceProvider resourceProvider,
                                 SchedulerProvider schedulerProvider, UiEvents uiEvents, BooksManager booksManager) {
        super(resourceProvider, schedulerProvider, uiEvents);
        this.booksManager = booksManager;
        retrieveDataFromNetwork();
    }

    @SuppressLint("CheckResult")
    public void retrieveDataFromNetwork() {

        booksManager.getBooks()
                .doOnSubscribe(this::addDisposable)
                .subscribeOn(Schedulers.io())
                .map(BooksPresentationMapper.INSTANCE::listOfAppsDomainToPresentation)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(books -> {
                     booksObservable.accept(books);
                }, error -> {
                    Timber.e(error);
                });
    }

    @Override
    public int getTitleVisibility() {
        return View.GONE;
    }

    @Override
    public int getImageVisibility() {
        return View.VISIBLE;
    }

    public Observable<List<BookPresentation>> observeBookSearch() {
        return booksObservable;
    }

}