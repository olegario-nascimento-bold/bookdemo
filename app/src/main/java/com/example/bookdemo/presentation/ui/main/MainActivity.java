package com.example.bookdemo.presentation.ui.main;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.example.bookdemo.R;
import com.example.bookdemo.databinding.ActivityMainBinding;
import com.example.bookdemo.presentation.base.toolbar.BaseToolbarActivity;
import com.example.bookdemo.presentation.ui.main.home.HomeFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.example.bookdemo.utils.pagination.ServicesUtils;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.ncapdevi.fragnav.tabhistory.UnlimitedTabHistoryStrategy;


public class MainActivity extends BaseToolbarActivity<ActivityMainBinding, MainViewModel> {

    public static void launch(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Inject
    ServicesUtils mServicesUtils;

    private FragNavController fragNavController = new FragNavController(getSupportFragmentManager(), R.id.fragment_container);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNavigation(savedInstanceState);
    }

    private void initNavigation(Bundle savedInstanceState) {
        fragNavController.setFragmentHideStrategy(FragNavController.DETACH_ON_NAVIGATE_HIDE_ON_SWITCH);

        List<Fragment> fragments = new ArrayList<Fragment>(5) {{
            add(HomeFragment.newInstance());
        }};

        FragNavTransactionOptions options = new FragNavTransactionOptions.Builder()
                .transition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .build();
        fragNavController.setDefaultTransactionOptions(options);

        fragNavController.setRootFragments(fragments);

        fragNavController.initialize(FragNavController.TAB1, savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragNavController.onSaveInstanceState(outState);
    }

    @Override
    public int layoutResId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews(ActivityMainBinding binding) {
    }

    @SuppressLint("CheckResult")
    @Override
    protected void registerObservables() {
        super.registerObservables();
    }

    @Override
    public Class<MainViewModel> viewModelClass() {
        return MainViewModel.class;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        final boolean isStateSaved = fragmentManager.isStateSaved();

        if (isStateSaved && Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1) {
            // Older versions will throw an exception from the framework
            // FragmentManager.popBackStackImmediate(), so we'll just
            // return here. The Activity is likely already on its way out
            // since the fragmentManager has already been saved.
            return;
        }
    }

}