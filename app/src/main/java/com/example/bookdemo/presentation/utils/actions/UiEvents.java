package com.example.bookdemo.presentation.utils.actions;


import android.text.Spannable;
import android.util.Pair;

import com.example.bookdemo.presentation.utils.actions.events.CloseEvent;
import com.example.bookdemo.presentation.utils.actions.events.ToastEvent;
import com.jakewharton.rxrelay2.PublishRelay;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class UiEvents {

    private PublishRelay<ToastEvent> mToastRelay = PublishRelay.create();
    private PublishRelay<CloseEvent> mCloseRelay = PublishRelay.create();
    private PublishRelay<Pair<String, UiMessages.BooleanDialogCallback>> mBooleanDialogRelay = PublishRelay.create();
    private PublishRelay<Pair<String, String>> mOkDialogRelay = PublishRelay.create();
    private PublishRelay<Pair<String, Spannable>> mOkSpannableDialogRelay = PublishRelay.create();

    @Inject
    public UiEvents() {}

    public void close() {
        mCloseRelay.accept(new CloseEvent());
    }

    public void showToast(String message) {
        if (message != null && !message.isEmpty()) {
            mToastRelay.accept(new ToastEvent(message));
        }
    }

    public void showDialog(String title, String message) {
        mOkDialogRelay.accept(Pair.create(title, message));
    }

    public void showSpannableDialog(String title, Spannable message) {
        mOkSpannableDialogRelay.accept(Pair.create(title, message));
    }

    public void showBooleanDialog(String message, UiMessages.BooleanDialogCallback callback) {
        mBooleanDialogRelay.accept(Pair.create(message, callback));
    }

    public Observable<ToastEvent> observeToastEvent() {
        return mToastRelay;
    }

    public Observable<CloseEvent> observeCloseEvent() {
        return mCloseRelay;
    }

    public Observable<Pair<String, String>> observeDialogEvent() {
        return mOkDialogRelay;
    }

    public Observable<Pair<String, Spannable>> observeSpannableDialogEvent() {
        return mOkSpannableDialogRelay;
    }

    public Observable<Pair<String, UiMessages.BooleanDialogCallback>> observeBooleanDialogEvent() {
        return mBooleanDialogRelay;
    }
}