package com.example.bookdemo.presentation.base.toolbar;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.databinding.Bindable;

import com.example.bookdemo.R;
import com.example.bookdemo.common.schedulers.SchedulerProvider;
import com.example.bookdemo.presentation.base.BaseViewModel;
import com.example.bookdemo.presentation.utils.actions.UiEvents;
import com.example.bookdemo.utils.ResourceProvider;

public abstract class BaseToolbarViewModel extends BaseViewModel {

    public BaseToolbarViewModel(ResourceProvider resourceProvider, SchedulerProvider schedulerProvider, UiEvents uiEvents) {
        super(resourceProvider, schedulerProvider, uiEvents);
    }

    @Bindable
    public String getToolbarTitle() {
        return "";
    }

    @Bindable
    public Drawable getToolbarImage() {
        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        return transparentDrawable;
    }

    @Bindable
    public int getToolbarTextColor() {
        return R.color.teal_200;
    }

    @Bindable
    public int getTitleVisibility() {
        return View.VISIBLE;
    }

    @Bindable
    public int getImageVisibility() {
        return View.GONE;
    }

    public void onAppLogoClicked() {

    }

    @Bindable
    public int getImageScale() {
        return 1;
    }

}