package com.example.bookdemo.presentation.ui.main.home;

import android.annotation.SuppressLint;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.bookdemo.R;
import com.example.bookdemo.databinding.FragmentHomeBinding;
import com.example.bookdemo.presentation.base.toolbar.BaseToolbarFragment;

public class HomeFragment extends BaseToolbarFragment<FragmentHomeBinding, HomeFragmentViewModel> {

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }
    private BooksAdapter adapter;

    @Override
    protected int layoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected Class<HomeFragmentViewModel> viewModelClass() {
        return HomeFragmentViewModel.class;
    }

    @Override
    protected void initViews(FragmentHomeBinding binding) {

        initToolbarWithoutNavigation(binding.actionbar.toolbar);

        binding.fragmentHomeAppsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.fragmentHomeAppsRecyclerView.addItemDecoration(new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL));

        adapter = new BooksAdapter();
        binding.fragmentHomeAppsRecyclerView.setAdapter(adapter);
    }

    @SuppressLint("CheckResult")
    @Override
    protected void registerObservables() {
        super.registerObservables();

        getViewModel().observeBookSearch()
                .doOnSubscribe(this::addDisposable)
                .subscribe(books -> adapter.submitList(books));

    }
}