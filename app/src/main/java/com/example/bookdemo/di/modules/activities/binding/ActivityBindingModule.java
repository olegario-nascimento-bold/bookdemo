package com.example.bookdemo.di.modules.activities.binding;


import com.example.bookdemo.di.modules.activities.MainActivityModule;
import com.example.bookdemo.di.modules.activities.SplashActivityModule;
import com.example.bookdemo.di.scopes.ActivityScope;
import com.example.bookdemo.presentation.ui.main.MainActivity;
import com.example.bookdemo.presentation.ui.splash.SplashScreenActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mMainActivity();

    @ActivityScope
    @ContributesAndroidInjector(modules = SplashActivityModule.class)
    abstract SplashScreenActivity mSplashScreenActivity();


}