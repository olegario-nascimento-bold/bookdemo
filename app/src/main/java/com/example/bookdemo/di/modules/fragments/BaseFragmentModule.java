package com.example.bookdemo.di.modules.fragments;

import androidx.fragment.app.Fragment;

import dagger.Binds;
import dagger.Module;

@Module
abstract class BaseFragmentModule<F extends Fragment> {

    @Binds
    abstract Fragment fragment(F Fragment);
}