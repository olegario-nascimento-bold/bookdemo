package com.example.bookdemo.di.modules.fragments;

import com.example.bookdemo.presentation.ui.main.home.HomeFragment;

import dagger.Module;

@Module
public abstract class HomeFragmentModule extends BaseFragmentModule<HomeFragment> {
}
