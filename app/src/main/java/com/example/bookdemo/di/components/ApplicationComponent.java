package com.example.bookdemo.di.components;

import com.example.bookdemo.BookApp;
import com.example.bookdemo.di.modules.ApplicationModule;
import com.example.bookdemo.di.modules.NetworkModule;
import com.example.bookdemo.di.modules.activities.binding.ActivityBindingModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        NetworkModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class
})
public interface ApplicationComponent extends AndroidInjector<BookApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<BookApp> {
    }
}