package com.example.bookdemo.di.modules;


import android.app.Application;
import android.content.Context;
import android.location.LocationManager;

import com.example.bookdemo.BookApp;
import com.example.bookdemo.common.schedulers.AppSchedulerProvider;
import com.example.bookdemo.common.schedulers.SchedulerProvider;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module
public abstract class ApplicationModule {

    @Binds
    abstract Application application(BookApp app);

    @Provides
    static Context provideContext(Application application) {
        return application;
    }

    @Provides
    static SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }
}