package com.example.bookdemo.di.modules.activities;


import com.example.bookdemo.di.modules.fragments.HomeFragmentModule;
import com.example.bookdemo.di.scopes.FragmentScope;
import com.example.bookdemo.presentation.ui.main.MainActivity;
import com.example.bookdemo.presentation.ui.main.home.HomeFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule extends BaseActivityModule<MainActivity> {
    @FragmentScope
    @ContributesAndroidInjector(modules = HomeFragmentModule.class)
    abstract HomeFragment homeFragmentSubInjector();
}