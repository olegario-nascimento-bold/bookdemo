package com.example.bookdemo.di.modules.activities;

import com.example.bookdemo.presentation.ui.splash.SplashScreenActivity;

import dagger.Module;

@Module
public abstract class SplashActivityModule extends BaseActivityModule<SplashScreenActivity> {
}