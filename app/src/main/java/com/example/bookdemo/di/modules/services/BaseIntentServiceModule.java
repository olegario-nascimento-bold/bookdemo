package com.example.bookdemo.di.modules.services;

import android.app.IntentService;

import dagger.Binds;
import dagger.Module;

@Module
abstract class BaseIntentServiceModule<S extends IntentService> {

    @Binds
    abstract IntentService service(S service);

}