package com.example.bookdemo.utils.binding;


import android.graphics.drawable.Drawable;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;

import com.example.bookdemo.presentation.GlideApp;

public class BindingUtils {

    @InverseBindingAdapter(attribute = "android:checked")
    public static boolean get(CompoundButton compoundButton) {
        return compoundButton.isChecked();
    }

    @BindingAdapter(value = {"imageUrl", "placeholderResIdUrl", "placeholderDrawableUrl"}, requireAll = false)
    public static void setImageUrl(ImageView imageView, String url, int placeholderResIdUrl, Drawable placeholderDrawableUrl) {
        //url = Utils.normalizeUrl(url);
        if (placeholderResIdUrl > 0) {

            GlideApp.with(imageView.getContext())
                    .load(url)
                    .placeholder(placeholderResIdUrl)
                    .into(imageView);
        } else {
            GlideApp.with(imageView.getContext())
                    .load(url)
                    .placeholder(placeholderDrawableUrl)
                    .into(imageView);
        }
    }
}
